/*
* support_LED.c
*
* Created: 27/02/2021 18:00:59
*  Author: OKNO-8
*/

#include <avr/io.h>
#include <util/delay.h>

#include "support_LED.h"


void support_LED_pins_init() {
	
	/* Set support LED's pins
	* PC0 : LED
	* PC1 : Tact switch {pull up}
	*/
	DDRC &= 0xFD;
	DDRC |= 0x01;
	
	PORTC &= 0xFE;
	PORTC |= 0x02;
}

void set_support_LED() {
	
	PORTC |= 0x01;
}

void toggle_support_LED() {
	
	//if ((PINC & 0x02) == 0x00) {
		//
		//// Service tact switch
		//_delay_ms(20);
		//while ((PINC & 0x02) == 0x00) {}
		//_delay_ms(20);
		//
		////Indicate service tact switch wherewithal toggle LED
		PORTC ^= 0x01;
		//SPI_transmit_data((SPI_debugger_data){.LSB_bytes = 0x1234, .MSB_bytes = 0xABCD});
	//}
}