/*
 * support_LED.h
 *
 * Created: 27/02/2021 18:00:51
 *  Author: OKNO-8
 */ 

/* Weigh down pins :
* PC0 (LED)
* PC1 (Tact switch) {pull up}
*/

#ifndef SUPPORT_LED_H_
#define SUPPORT_LED_H_


void support_LED_pins_init();

void set_support_LED();

void toggle_support_LED();

#endif /* SUPPORT_LED_H_ */