/*
* motor_driver.h
*
* Created: 26/02/2021 12:05:59
*  Author: OKNO-8
*
*
* Weight down counters :
* TC0
*
* Weigh down pins :
* PC2 (Step)
* PC3 (Dir) {default consistent with clock's direction}
* PC4 (motor's switch) {pull up}
* PC5 (motor_reset) {pull up}
*
* PD0 (motor_stop) {pull up}
* PD1 (motor_continue) {pull up}
* PD2 (reset_sync)
*/


#ifndef MOTOR_DRIVER_H_
#define MOTOR_DRIVER_H_

#include <avr/interrupt.h>


enum Motor_direction {
	
	Left, Right
};

// Motor control's states signals
volatile struct {

	_Bool motor_reset;
	_Bool motor_stop;
} Motor_driver_control_signals;


// Service motor reset's interrupt
ISR (PCINT1_vect);

// Service motor stop's interrupt
ISR (PCINT2_vect);

// Init motor driver's modules
void motor_driver_init();

void motor_driver_pins_init();

void motor_driver_control_signals_init();

void motor_driver_unlock_control_pins_interrupts();

void set_motor_driver_TC0();

// Check motor driver's control signals and reset respectively state signals
void check_motor_driver_control_signals
(uint8_t *const scans_guard, enum Motor_direction *const motor_direction);

/* Motor step ahead with parameters:
* 18 degree step
* ~246 rev/min (~25.83 rad/s)
*/
void motor_step_ahead();

void set_motor_direction(enum Motor_direction motor_direction);

/* Reset motor's position & set default motor's direction (right) with parameters:
* 18 degree step
* ~246 rev/min (~25.83 rad/s)
*/
void reset_motor_position();

#endif /* MOTOR_DRIVER_H_ */