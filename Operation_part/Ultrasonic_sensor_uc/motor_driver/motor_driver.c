/*
* motor_driver.c
*
* Created: 26/02/2021 12:06:09
*  Author: OKNO-8
*/

#include <avr/io.h>
#include <util/delay.h>

#include "../SPI_transmitter/SPI_transmitter.h"
#include "../ultrasonic_sensor/ultrasonic_sensor.h"
#include "../motor_driver/motor_driver.h"
#include "../support_LED/support_LED.h"
#include "../Measurement_controler/Measurement_controler.h"


ISR (PCINT1_vect) {
	
	/* Lock pins interrupts
	* PC5 : motor_reset {pull up}
	* PD0 : motor_stop {pull up}
	*/
	PCMSK1 &= 0xDF;
	PCMSK2 &= 0xFE;
	
	// Set motor reset's state signal
	Motor_driver_control_signals.motor_reset = 1;
}

ISR (PCINT2_vect) {
	
	/* Lock pins interrupts
	* PC5 : motor_reset {pull up}
	* PD0 : motor_stop {pull up}
	*/
	PCMSK1 &= 0xDF;
	PCMSK2 &= 0xFE;
	
	// Set motor stop's state signal
	Motor_driver_control_signals.motor_stop = 1;
}

void motor_driver_init() {

	// Init motor driver's modules
	motor_driver_pins_init();
	
	motor_driver_unlock_control_pins_interrupts();

	motor_driver_control_signals_init();
}

void motor_driver_pins_init() {
	
	/* Set motor driver's pins
	* PC2 : step
	* PC3 : dir {default consistent with clock's direction}
	* PC4 : motor's switch {pull up}
	* PC5 : motor_reset {pull up}
	*
	* PD0 : motor_stop {pull up}
	* PD1 : motor_continue {pull up}
	* PD2 : reset_sync
	*/
	DDRC &= 0xCF;
	DDRC |= 0x0C;
	
	PORTC &= 0xFB;
	PORTC |= 0x38;
	
	DDRD &= 0xFC;
	DDRD |= 0x04;
	PORTD |= 0x07;
}

void motor_driver_control_signals_init() {
	
	// Init motor driver control signals states
	Motor_driver_control_signals.motor_reset = 0;
	Motor_driver_control_signals.motor_stop = 0;
}

void motor_driver_unlock_control_pins_interrupts() {
	
	/* Unlock pins interrupts
	* PC5 : motor_reset {pull up}
	*
	* PD0 : motor_stop {pull up}
	*/
	
	PCICR |= 0x06;
	PCMSK1 |= 0x20;
	PCMSK2 |= 0x01;
}

void set_motor_driver_TC0() {
	
	/* ### Set TC0 (8 bit counter) ###
	* mode : CTC
	* prescaler : 1024 (one impuls is 64us)
	*/
	TCCR0A = 0x02;
	TCCR0B = 0x05;
}

void check_motor_driver_control_signals
(uint8_t *const scans_guard, enum Motor_direction *const motor_direction) {
	
	// Check motor reset's interrupt trigger case
	if (Motor_driver_control_signals.motor_reset == 1) {
		
		// Reset motor position
		reset_motor_position();
		
		// Reset main's state signals
		*scans_guard = 0;
		*motor_direction = Right;
		
		// Signal reset
		set_support_LED();
		
		// Delay for service event wherewithal driver part
		_delay_us(100);
		
		// Signal end of reset system
		PORTD &= 0xFB;
		_delay_us(1);
		PORTD |= 0x04;
		
		// Wait for start system (push motor_continue signal)
		while ((PIND & 0x02) == 0x02) {}
		
		// Service tact switch
		_delay_ms(10);
		while ((PIND & 0x02) == 0x00) {}
		_delay_ms(10);
		
		/* Unlock pins interrupts
		* PC5 : motor_reset {pull up}
		* PD0 : motor_stop {pull up}
		*/
		PCMSK1 |= 0x20;
		PCMSK2 |= 0x01;
		
		// Reset motor reset state's signal
		Motor_driver_control_signals.motor_reset = 0;
		
		// Measurement sequence to synchronize system's work
		measurement_sequence(scans_guard, motor_direction);
	}
	// Check motor_stop's interrupt trigger case
	if (Motor_driver_control_signals.motor_stop == 1) {
		
		// Reset motor stop state's signal
		Motor_driver_control_signals.motor_stop = 0;
		
		// Signal stop
		set_support_LED();
		
		//Wait for continuation system's work signal (push motor_continue signal)
		while ((PIND & 0x02) == 0x02) {}
		
		// Service tact switch
		_delay_ms(10);
		while ((PIND & 0x02) == 0x00) {}
		_delay_ms(10);
		
		/* Unlock pins interrupts
		* PC5 : motor_reset {pull up}
		* PD0 : motor_stop {pull up}
		*/
		PCMSK1 |= 0x20;
		PCMSK2 |= 0x01;
	}
}

void motor_step_ahead() {
	
	// Set TC0 counter
	set_motor_driver_TC0();
	
	// Step signal's rising edges
	for (uint8_t i = 0; i < 10; ++i) {

		// Propagate step's rising edge
		PORTC |= 0x04;

		// ### Propagate step signal with delay (12.16ms) ###
		
		// Start TC0 count up
		TCNT0 = 0x00;

		while (TCNT0 != 190) {}
		
		// Propagate step's falling edge
		PORTC &= 0xFB;
	}
}

void set_motor_direction(enum Motor_direction motor_direction) {
	
	// Set respectively motor's direction
	switch (motor_direction) {
		case Left: {
			
			PORTC &= 0xF7;
		}
		break;
		case Right: {
			
			PORTC |= 0x08;
		}
		break;
	}
}

void reset_motor_position() {
	
	// Set TC0 counter
	set_motor_driver_TC0();
	
	// Set motor's direction
	set_motor_direction(Left);
	
	// Steps until motor compass base position (speed ~246 rev/min) {delay = 12.16ms}
	while ((PINC & 0x10) == 0x10) {
		
		// Step's rising edge
		PORTC |= 0x04;
		
		// Start TC0 count up
		TCNT0 = 0x00;
		
		// ### Propagate step signal with delay (12.16ms) ###
		while (TCNT0 != 190) {}
		
		// Step's falling edge
		PORTC &= 0xFB;
	}
	
	// Set default motor's direction
	set_motor_direction(Right);
}