/*
* Measurement_controler.h
*
* Created: 10.03.2021 20:23:58
*  Author: OKNO1010B
*/


#ifndef MEASUREMENT_CONTROLER_H_
#define MEASUREMENT_CONTROLER_H_

#include <stdint.h>
#include "../motor_driver/motor_driver.h"


void measurement_sequence(uint8_t *const scans_guard,
enum Motor_direction *const motor_direction);

#endif /* MEASUREMENT_CONTROLER_H_ */