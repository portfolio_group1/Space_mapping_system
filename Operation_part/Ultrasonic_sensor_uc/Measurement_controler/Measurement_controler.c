/*
* Measurement_controler.c
*
* Created: 10.03.2021 20:25:22
*  Author: OKNO1010B
*/

#include <util/delay.h>

#include "Measurement_controler.h"
#include "../motor_driver/motor_driver.h"
#include "../SPI_transmitter/SPI_transmitter.h"
#include "../ultrasonic_sensor/ultrasonic_sensor.h"


void measurement_sequence(uint8_t *const scans_guard,
enum Motor_direction *const motor_direction) {
	
	// Work signal's LED
	toggle_support_LED();
	
	// Single ultrasonic sensor's measurement
	ultrasonic_sensor_measurement();
	
	// Define spi debugger data container
	SPI_debugger_data spi_debugger_data;
	
	// Fill spi debugger data container the measurement time's value
	spi_debugger_data.bytes[0] = measurement_time;
	
	// Fill spi debugger data container the scans guard's value
	if ((*motor_direction) == Right) {
		
		spi_debugger_data.bytes[1] = (*scans_guard);
	}
	if ((*motor_direction) == Left) {
	
		spi_debugger_data.bytes[1] = (10 - (*scans_guard));
	}
	
	/* Send measurement information by SPI's bus
	* (global variable from ultrasonic_sensor's header file)
	*/
	SPI_transmit_data(&spi_debugger_data);
	
	// Delay between scans
	_delay_ms(1500);
	
	// Check motor control's signals
	check_motor_driver_control_signals(scans_guard, motor_direction);
}