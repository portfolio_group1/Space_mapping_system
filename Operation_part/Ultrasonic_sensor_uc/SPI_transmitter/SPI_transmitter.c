/*
* SPI_debugger.c
*
* Created: 14/02/2021 14:23:01
*  Author: OKNO-8
*/

#include "SPI_transmitter.h"
#include <util/delay.h>
#include <avr/io.h>


void SPI_transmitter_init() {
	
	// Init SPI's pins
	DDRB |= 0x2C;
	DDRB &= 0xEF;
	
	/* Set SPI's PORTB
	* Set CS as high
	*/
	PORTB &= 0xC7;
	PORTB |= 0x04;
	
	/* Set SPI
	* Big endian frame's data
	* Master mode
	* Polarity = high
	* Phase = 2 edge
	* Prescaler f_cpu / 4
	*/
	SPCR |= 0x5C;
	SPSR &= 0x00;
}

void SPI_transmit_data(const SPI_debugger_data *const spi_debugger_data) {

	// Begin transmit data's frame
	PORTB &= 0xFB;
	
	for (uint8_t i = 0; i < SPI_transmitter_data_size; ++i) {

		// Begin transmit Byte
		SPDR = (spi_debugger_data->bytes[i]);
		
		// Wait for close Byte transmission
		while ((SPSR & 0x80) == 0x00) {}
	}
	
	// Delay between transfer frames sequence
	_delay_us(300);
	
	// End transmission
	PORTB |= 0x04;
}