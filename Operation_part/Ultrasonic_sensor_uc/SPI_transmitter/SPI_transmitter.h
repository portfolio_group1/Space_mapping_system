/*
* SPI_debugger.h
*
* Created: 14/02/2021 14:22:47
*  Author: OKNO-8
*/

#ifndef SPI_DEBUGGER_H_
#define SPI_DEBUGGER_H_

#include <stdint.h>


// SPI transmitter data size [Bytes]
#define SPI_transmitter_data_size 2

typedef union {
	
	uint16_t data;
	uint8_t bytes[SPI_transmitter_data_size];
} SPI_debugger_data;

void SPI_transmitter_init();

void SPI_transmit_data(const SPI_debugger_data *const spi_debugger_data);

#endif /* SPI_DEBUGGER_H_ */