/*
* init_ultrasonic_sensor.c
*
* Created: 03/02/2021 15:29:30
*  Author: OKNO-8
*/

#include <util/delay.h>

#include "ultrasonic_sensor.h"


// Echo transmit state's data structure
volatile _Bool end_ultrasonic_sensor_measurement;
volatile uint8_t measurement_start_time;


ISR (PCINT0_vect) {

	// Immediately get hardware timer 1's bottom byte
	measurement_time = TCNT1L;

	// Lock TC1's channel A compare match interrupt
	TIMSK1 &= 0xFD;

	// Get data in end of echo time's measurement
	if ((PINB & 0x01) == 0x00) {

		// Lock echo (PB0) pin change interrupt
		PCICR &= 0xFE;
		PCMSK0 &= 0xFE;
		
		// Check timer overflov
		if ((measurement_time - measurement_start_time) < 0) {

			// Correct measurement's time value
			measurement_time = ((0xFF - measurement_start_time) + measurement_time);
		}
		else {

			// Correct measurement's time value
			measurement_time = (measurement_time - measurement_start_time);
		}

		/* Compare measurement with max value
		* 92us = 100cm
		*/
		if (measurement_time > 92) {
			
			//measurement_time = 92;
			measurement_time = 0xFF;
		}
		
		// Set end ultrasonic sensor's measurement state signal
		end_ultrasonic_sensor_measurement = 1;
	}
	// Start echo time's measurement
	else if ((PINB & 0x01) == 0x01) {

		// Get measurement start time's value
		measurement_start_time = measurement_time;
		
		// Set top TC1's channel A value
		OCR1A = (TCNT1 + 0x0100);
		//OCR1A = (TCNT1 + 0xFF);
		
		// Unlock TC1's channel A compare match interrupt
		TIFR1 |= 0x02;
		TIMSK1 |= 0x02;
	}
}

ISR (TIMER1_COMPA_vect) {
	
	// Lock echo (PB0) pin change interrupt
	PCICR &= 0xFE;
	PCMSK0 &= 0xFE;
	
	// Lock TC1's channel A compare match interrupt
	TIMSK1 &= 0xFD;
	
	// Set max value to measurement time
	measurement_time = 0xFF;
	
	// Set end ultrasonic sensor's measurement state signal
	end_ultrasonic_sensor_measurement = 1;
}

void ultrasonic_sensor_init() {
	
	// Init ultrasonic sensor's pins
	ultrasonic_sensor_pins_init();
	
	// Init ultrasonic sensor's data
	end_ultrasonic_sensor_measurement = 0;
	measurement_time = 0;
}

void ultrasonic_sensor_pins_init() {
	
	/* Set ultrasonic pins
	* PB0 : echo
	* PB1 : trig
	*/
	DDRB &= 0xFE;
	DDRB |= 0x02;
	
	PORTB &= 0xFC;
}

void ultrasonic_sensor_set_counters() {
	
	// Set hardware timer 1
	set_ultrasonic_sensor_counter_TC1_prsc_1024();
}

void set_ultrasonic_sensor_counter_TC1_prsc_1024() {
	
	/* ### Set TC1 (16 bit counter) ###
	* mode : CTC
	* Prescaler set to clk / 1024
	*/
	TCCR1A = 0x00;
	TCCR1B = 0x0D;
	TCCR1C = 0x00;
	
	// Turn off TC1's channel A interrupt
	TIMSK1 &= 0xFD;
}

void ultrasonic_sensor_measurement() {
	
	// Set ultrasonic sensor's counters
	ultrasonic_sensor_set_counters();
	
	// Unlock echo (PB0) pin change interrupt
	PCICR |= 0x01;
	PCMSK0 |= 0x01;

	// Begin communication (10 us HIGH in trig pin)
	//TCNT0 = 0x00;
	PORTB |= 0x02;
	
	// Check TC0 channel A countdown to 20 (10 us)
	_delay_us(10);
	//while (TCNT0 != 20) {}
	
	// Finish propagate HIGH in trig pin
	PORTB &= 0xFD;
	
	// Wait for finish measurement
	while (end_ultrasonic_sensor_measurement == 0) {}
	
	// Reset end ultrasonic sensor measurement's state signal
	end_ultrasonic_sensor_measurement = 0;

	// TODO return measurement value
}