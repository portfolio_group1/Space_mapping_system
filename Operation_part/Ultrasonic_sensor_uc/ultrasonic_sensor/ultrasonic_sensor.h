/*
* init_ultrasonic_sensor.h
*
* Created: 03/02/2021 15:29:13
*  Author: OKNO-8

* Max extent : 100 cm (max_error_L = 1.088 cm)
*
* Weight down counters :
* TC0 (First 10us measurement's time)
* TC1 (channel A's interrupt)
*
* Weigh down pins :
* PB0 (Echo)
* PB1 (Trig)
*/


#ifndef INIT_ULTRASONIC_SENSOR_H_
#define INIT_ULTRASONIC_SENSOR_H_

#include <avr/interrupt.h>
#include "measurement_converter.h"

// Ultrasonic sensor measurement's data
volatile uint8_t measurement_time;


// Echo input capture interrupt
ISR (PCINT0_vect);

// Echo pin's signal counter overflow
ISR (TIMER1_COMPA_vect);

void ultrasonic_sensor_init();

void ultrasonic_sensor_pins_init();

void ultrasonic_sensor_set_counters();

void set_ultrasonic_sensor_counter_TC1_prsc_1024();

// TODO measurement with transmit frame
void ultrasonic_sensor_measurement();

#endif /* INIT_ULTRASONIC_SENSOR_H_ */