//
// Created by Michał Kopiel on 03/02/2021.
//

#ifndef UNTITLED_MEASUREMENT_CONVERTER_H
#define UNTITLED_MEASUREMENT_CONVERTER_H

#include <stdint.h>

/* Distance data structure
 * Max value = 100.00
 * Precision :
 * three attribute's digits
 * two mantissa's digits
 *
 * Motor's angle is defined as amount of steps from left system scan's side
 * (it field is fill just before SPI's transmission in main function)
 * */
typedef struct {

    char measurement_attribute[4];
    char measurement_mantissa[3];
    uint8_t motor_angle;
} Distance_data;


void measurement_mantissa_scaling(char *const measurement_mantissa);

Distance_data measurement_converter(const uint8_t measurement_time);

#endif //UNTITLED_MEASUREMENT_CONVERTER_H