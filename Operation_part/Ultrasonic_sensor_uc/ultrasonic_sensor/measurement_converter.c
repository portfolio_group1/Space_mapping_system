//
// Created by Michał Kopiel on 03/02/2021.
//

#include "measurement_converter.h"
#include <string.h>
#include <stdio.h>


void measurement_mantissa_scaling(char *const measurement_mantissa) {

    // Scan measurement mantissa's MSB value
    if (measurement_mantissa[0] == ' ') {

        // Swap measurement mantissa's MSB value as space character to zero value
        measurement_mantissa[0] = '0';
    }
}

Distance_data measurement_converter(const uint8_t measurement_time) {

    // Distance data's structure
    Distance_data distance_data;


    // ### Object's not detected case ###
    if (measurement_time == 0xFF) {

        // Fill entire measurement value the F value
        strcpy(distance_data.measurement_attribute, "FFF\0");
        strcpy(distance_data.measurement_mantissa, "FF\0");

        // Return distance data structure
        return distance_data;
    }

    // ### Object's detected case ###
    // Calculate base measurement value
    uint32_t measurement_value = ((measurement_time << 6) * 17);

    // Calculate and convert measurement's attribute
    sprintf(distance_data.measurement_attribute, "%3d",
            (measurement_value / 1000));

    // Calculate and convert measurement's mantissa
    sprintf(distance_data.measurement_mantissa, "%2d",
            ((measurement_value % 1000) / 10));

    // Scaling mantissa's value
    measurement_mantissa_scaling(distance_data.measurement_mantissa);

    // Return distance data structure
    return distance_data;
}