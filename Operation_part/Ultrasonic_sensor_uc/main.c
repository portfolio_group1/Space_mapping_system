/*
* Ultrasonic_sensor_uc.c
*
* Created: 03/02/2021 15:01:47
* Author : OKNO-8
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "SPI_transmitter/SPI_transmitter.h"
#include "ultrasonic_sensor/ultrasonic_sensor.h"
#include "motor_driver/motor_driver.h"
#include "support_LED/support_LED.h"


void measurement_sequence();

int main(void)
{
	// Init ports
	DDRB = 0x00;
	DDRC = 0x00;
	DDRD = 0x00;

	PORTB = 0x00;
	PORTC = 0x00;
	PORTD = 0x00;
	
	// Unlock interrupts
	sei();
	
	// Init SPI transmitter
	SPI_transmitter_init();

	// Init ultrasonic sensor
	ultrasonic_sensor_init();
	
	// Init motor driver
	motor_driver_init();
	
	// Init support LED's pins
	support_LED_pins_init();
	
	
	/* ###### Start loop system ###### */
	
	// Scans guard register
	uint8_t scans_guard = 0;
	
	// Actual motor's direction information
	enum Motor_direction motor_direction = Right;
	
	// Signal before start system
	set_support_LED();
	
	// Reset motor's position
	reset_motor_position();
	
	// Delay for turn on driver part
	_delay_us(100);
	
	// wait for start system (push motor_continue signal)
	while ((PIND & 0x02) == 0x02) {}
	
	while (1) {
		
		// Measurement sequence
		measurement_sequence(&scans_guard, &motor_direction);
		
		// Motor's step ahead
		motor_step_ahead();
		
		// Check motor compass last position case
		if (++scans_guard == 10) {
			
			// Change motor's direction
			motor_direction = (!motor_direction);
			set_motor_direction(motor_direction);
			
			// Reset scans guard register
			scans_guard = 0;
		}
	}
}