/*
* Interrupts_controller.c
*
* Created: 06/03/2021 20:57:12
*  Author: OKNO1010B
*/

#include <util/delay.h>
#include "../System_controller/System_controller.h"
#include "Interrupts_controller.h"


ISR(PCINT1_vect) {
	
	// Motor reset case
	if ((PINC & 0x20) == 0x00) {
		
		/* Lock user control panel's pins interrupts
		* PC4 : motor_reset {pull up}
		* PC5 : motor_stop {pull up}
		*/
		PCMSK1 &= 0xCF;
		
		// Service tact switch
		_delay_ms(10);
		while((PINC & 0x20) == 0x00) {}
		_delay_ms(10);

		// Set reset system's state signal
		User_control_panel_signals.reset_system = 1;
	}
	// Motor stop case
	if ((PINC & 0x10) == 0x00) {
		
		/* Lock user control panel's pins interrupts
		* PC4 : motor_reset {pull up}
		* PC5 : motor_stop {pull up}
		*/
		PCMSK1 &= 0xCF;
		
		// Service tact switch
		_delay_ms(10);
		while((PINC & 0x10) == 0x00) {}
		_delay_ms(10);
		
		// Set stop system's work state signal
		User_control_panel_signals.stop_system = 1;
	}
}

ISR(SPI_STC_vect) {
	
	// Get SPI's frame & increment frames counter
	SPI_data.SPI_frames[SPI_data.SPI_frames_counter++] = SPDR;
}