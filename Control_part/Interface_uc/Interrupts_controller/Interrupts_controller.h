/*
* Interrupts_controller.h
*
* Created: 06/03/2021 20:57:20
*  Author: OKNO1010B
*/


#ifndef INTERRUPTS_CONTROLLER_H_
#define INTERRUPTS_CONTROLLER_H_

#include <avr/interrupt.h>


// User control panel's states signals
volatile struct {
	
	_Bool reset_system;
	_Bool stop_system;
} User_control_panel_signals;


// Service user control panel pins's interrupts
ISR(PCINT1_vect);


// SPI's interrupt data structure
#define SPI_frames_size 2

volatile struct {

	uint8_t SPI_frames_counter;
	uint8_t SPI_frames[SPI_frames_size];
} SPI_data;


// Service SPI's interrupts
ISR(SPI_STC_vect);

#endif /* INTERRUPTS_CONTROLLER_H_ */