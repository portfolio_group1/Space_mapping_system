/*
* LCD_driver.c
*
* Created: 06/03/2021 18:51:23
*  Author: OKNO1010B
*/

#include "LCD_driver.h"
#include <stdlib.h>
#include "../Industry_HD44780/HD44780_driver.h"


void init_radar_characters() {
	
	// Define radar's characters
	const uint8_t radar_characters[8][8] =
	{
		{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF},
		{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF},
		{0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF},
		{0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF},
		{0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF},
		{0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF},
		{0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF},
		{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}
	};
	
	// Upload radar's characters
	for (uint8_t i = 0; i < 8; ++i) {

		lcd_defchar(i, &radar_characters[i][0]);
	}
}

void init_radar_stack() {
	
	// Init radar stack
	for (uint8_t i = 0; i < radar_stack_size; ++i) {
		
		radar_stack[i] = 0xFF;
	}
}

void clear_radar_stack() {
	
	// Clear radar stack
	for (uint8_t i = 0; i < radar_stack_size; ++i) {
		
		radar_stack[i] = 0xFF;
	}
}

void init_interface_lcd() {
	
	/* Set lcd driver's pins
	* PD0 : data4
	* PD1 : data5
	* PD2 : data6
	* PD3 : data7
	* PD4 : enable
	* PD5 : rw
	* PD6 : rs
	*/
	lcd_init();
	
	// Init radar's characters
	init_radar_characters();
	
	// Init radar's stack
	init_radar_stack();
}

void show_measurement_state(const Distance_data *const distance_data) {
	
	// Object deteced case
	if (distance_data->object_detected == 1) {
		
		// Measurement in fixed point form data
		char measurement_fixed_point_data[16];
		
		// Convert distance data container to LCD form
		sprintf(measurement_fixed_point_data, "Dystans: %s%c%s",
		distance_data->measurement_attribute, '.', distance_data->measurement_mantissa);
		
		// Clear previous distance informations
		lcd_cls();
		
		// ### Presentation measurement's data ###
		
		// Add information about detected objects to radar's stack
		radar_stack[distance_data->motor_angle] = (distance_data->detected_object_distance);
		
		// Present radar stack's content
		for (uint8_t i = 0; i < radar_stack_size; ++i) {
			
			// Check detected object in respectively angle
			if (radar_stack[i] != 0xFF) {
				
				// Go to base position
				lcd_goto(i, 0);
				
				// Show detected object in respectively angle
				lcd_putchar(radar_stack[i]);
			}
		}
		
		// Show actual measurement statement
		lcd_goto(0, 1);
		lcd_puttext(measurement_fixed_point_data);
	}
	// Object not deteced case
	if (distance_data->object_detected == 0) {
		
		// Add information about not detected objects to radar's stack
		radar_stack[distance_data->motor_angle] = 0xFF;
		
		// Clear previous distance informations
		lcd_cls();
		
		// Present radar stack's content
		for (uint8_t i = 0; i < radar_stack_size; ++i) {
			
			// Check detected object in respectively angle
			if (radar_stack[i] != 0xFF) {
				
				// Go to base position
				lcd_goto(i, 0);
				
				// Show detected object in respectively angle
				lcd_putchar(radar_stack[i]);
			}
		}
		
		// Show actual measurement statement
		lcd_goto(3, 1);
		lcd_puttext("Niewykryty");
	}
}

void show_reset_system_statement() {
	
	// Clear LCD content
	lcd_cls();
	
	// Clear radar's stack
	clear_radar_stack();
	
	// Show respectively statement
	lcd_goto(2, 0);
	lcd_puttext("Resetowanie");
	
	lcd_goto(4, 1);
	lcd_puttext("Systemu");
}

void show_continuing_system_work_statement() {
	
	// Clear LCD content
	lcd_cls();
	
	// Clear radar's stack
	clear_radar_stack();
	
	// Show respectively statement
	lcd_goto(1, 0);
	lcd_puttext("Trwa wznawianie");
	
	lcd_goto(1, 1);
	lcd_puttext("pracy systemu");
}

void clear_LCD_content() {
	
	// Just clear LCD content
	lcd_cls();
}

