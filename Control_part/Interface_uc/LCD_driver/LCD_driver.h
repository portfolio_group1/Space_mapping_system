/*
* LCD_driver.h
*
* Created: 06/03/2021 18:51:29
*  Author: OKNO1010B
*
* Weight down pins :
* PD0 (data4)
* PD1 (data5)
* PD2 (data6)
* PD3 (data7)
* PD4 (enable)
* PD5 (rw)
* PD6 (rs)
*/


#ifndef LCD_DRIVER_H_
#define LCD_DRIVER_H_

#include <stdint.h>
#include "../Measurement_converter/Measurement_converter.h"


/* ###!!!### Privates functions ###!!!### */

/*///////////////////////////////////////////////////
	Init radar's characters
///////////////////////////////////////////////////*/
void init_radar_characters();

/*///////////////////////////////////////////////////
	Init radars stack for 0xFF (not found objects value)
///////////////////////////////////////////////////*/
void init_radar_stack();

/*///////////////////////////////////////////////////
	Clear radar's stack
///////////////////////////////////////////////////*/
void clear_radar_stack();


/* ###!!!### Library interface ###!!!### */

/*///////////////////////////////////////////////////
	Init interface's lcd
///////////////////////////////////////////////////*/
void init_interface_lcd();

/*///////////////////////////////////////////////////
	# Detected objects's stack
	# Distance is show in radar character's form (0 : 7) values
///////////////////////////////////////////////////*/

#define radar_stack_size 11
uint8_t radar_stack[radar_stack_size];

void show_measurement_state(const Distance_data *const distance_data);

void show_reset_system_statement();

void show_continuing_system_work_statement();

void clear_LCD_content();

#endif /* LCD_DRIVER_H_ */