/*
* User_control_panel.c
*
* Created: 06/03/2021 18:41:55
*  Author: OKNO1010B
*/

#include <avr/io.h>
#include <util/delay.h>

#include "User_control_panel.h"
#include "../System_controller/System_controller.h"
#include "../Interrupts_controller/Interrupts_controller.h"
#include "../LCD_driver/LCD_driver.h"


void user_control_panel_pins_init() {
	
	/* Set user control panel's pins
	* PC2 : reset_sync {pull up}
	* PC3 : motor continue {pull up}
	* PC4 : motor stop {pull up}
	* PC5 : motor_reset {pull up}
	*/
	DDRC &= 0xC3;
	PORTC |= 0x3C;
}

void user_control_panel_interrupts_unlock() {
	
	/* Unlock pin's interrupts
	* PC4 : motor stop {pull up}
	* PC5 : motor_reset {pull up}
	*/
	PCICR |= 0x02;
	PCMSK1 |= 0x30;
}

void user_control_panel_interrupts_variables_init() {
	
	// Init user control panel interrupts's variables
	User_control_panel_signals.reset_system = 0;
	User_control_panel_signals.stop_system = 0;
}

void scan_user_control_panel_instructions() {
	
	// Reset system work case
	if (User_control_panel_signals.reset_system == 1) {
		
		// Service reset system
		service_reset_system();
		
		// Reset user control panel's state signal
		User_control_panel_signals.reset_system = 0;
		
		/* Unlock user control panel pin's interrupts
		* PC4 : motor stop {pull up}
		* PC5 : motor_reset {pull up}
		*/
		PCMSK1 |= 0x30;
	}
	// Stop system work case
	if (User_control_panel_signals.stop_system == 1) {
		
		// Service stop system
		service_stop_system();
		
		// Reset user control panel's state signal
		User_control_panel_signals.stop_system = 0;
		
		/* Unlock user control panel pin's interrupts
		* PC4 : motor stop {pull up}
		* PC5 : motor_reset {pull up}
		*/
		PCMSK1 |= 0x30;
	}
}

void service_reset_system() {
	
	// Show reset system work statement
	show_reset_system_statement();
	
	// Wait for end of reset system work
	while ((PINC & 0x04) == 0x04) {}
	
	// Clear LCD content
	clear_LCD_content();
}

void service_stop_system() {
	
	//Wait for continuation system's work signal (push motor_continue signal)
	while((PINC & 0x08) == 0x08) {}
	
	// Service tact switch
	_delay_ms(10);
	while((PINC & 0x08) == 0x00) {}
	_delay_ms(10);
}