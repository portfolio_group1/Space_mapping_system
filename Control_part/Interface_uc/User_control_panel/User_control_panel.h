/*
 * User_control_panel.h
 *
 * Created: 06/03/2021 18:41:48
 *  Author: OKNO1010B
 *
 * weight down pins :
 * PC2 (reset_sync) {pull up}
 * PC3 (motor continue) {pull up}
 * PC4 (motor stop) {pull up}
 * PC5 (motor_reset) {pull up}
  */ 


#ifndef USER_CONTROL_PANEL_H_
#define USER_CONTROL_PANEL_H_


void user_control_panel_pins_init();

void user_control_panel_interrupts_unlock();

void user_control_panel_interrupts_variables_init();

/* ### Service user control panel instructions ### */
void scan_user_control_panel_instructions();

void service_reset_system();

void service_stop_system();

#endif /* USER_CONTROL_PANEL_H_ */