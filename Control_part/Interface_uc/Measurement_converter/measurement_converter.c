//
// Created by Michał Kopiel on 03/02/2021.
//

#include <string.h>
#include "Measurement_converter.h"


void measurement_mantissa_scaling(char *const measurement_mantissa) {

	// Scan measurement mantissa's MSB value
	if (measurement_mantissa[0] == ' ') {

		// Swap measurement mantissa's MSB value as space character to zero value
		measurement_mantissa[0] = '0';
	}
}

void measurement_converter(const uint8_t measurement_time, Distance_data *const distance_data) {

	// ### Object not detected case ###
	if (measurement_time == 0xFF) {

		// Signal not object detected case
		distance_data->object_detected = 0;
	}
	
	// ### Object detected case ###
	if (measurement_time != 0xFF) {

		// Calculate base measurement value
		uint32_t measurement_value = (((uint32_t)measurement_time << 6) * 17);

		// Measurement value less than 100 case
		if (measurement_time <= 91) {
			
			distance_data->detected_object_distance = (measurement_value / 12500);
		}
		// Measurement value greater than 100 case
		if (measurement_time > 91) {
			
			distance_data->detected_object_distance = 7;
		}
		

		// Calculate and convert measurement's attribute
		sprintf(distance_data->measurement_attribute, "%d", (measurement_value / 1000));

		// Calculate and convert measurement's mantissa
		sprintf(distance_data->measurement_mantissa, "%2d", (measurement_value % 1000) / 10);

		// Scaling mantissa's value
		measurement_mantissa_scaling(distance_data->measurement_mantissa);
		
		// Signal object detected case
		distance_data->object_detected = 1;
	}
}