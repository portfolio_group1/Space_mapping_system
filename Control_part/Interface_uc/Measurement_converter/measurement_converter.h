//
// Created by Michał Kopiel on 03/02/2021.
//

#ifndef UNTITLED_MEASUREMENT_CONVERTER_H
#define UNTITLED_MEASUREMENT_CONVERTER_H

#include <stdint.h>

/* Distance data structure
* Max value = 100.00
* Precision :
* three attribute's digits
* two mantissa's digits
*
* Object detected inform about detected object case (0 when measurement time equal 0xFF)
*
* Detected object distance represent value in radar character's form (0 : 7) values
* Motor's angle is defined as amount of steps from left system scan's side
* */
typedef struct {

	_Bool object_detected;
	
	 uint8_t detected_object_distance;
	
	char measurement_attribute[4];
	char measurement_mantissa[3];
	uint8_t motor_angle;
} Distance_data;


// Scaling measurement mantissa in measurement converter function
void measurement_mantissa_scaling(char *const measurement_mantissa);

// Convert measurement time to distance data structure & set object detected flag
void measurement_converter(const uint8_t measurement_time, Distance_data *const distance_data);

#endif //UNTITLED_MEASUREMENT_CONVERTER_H