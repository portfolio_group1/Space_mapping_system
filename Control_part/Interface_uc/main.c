/*
 * Interface_uc.c
 *
 * Created: 06/03/2021 17:45:50
 * Author : OKNO1010B
 */ 

#include <avr/io.h>
#include "System_controller/System_controller.h"


int main(void)
{
	// Default init pins
	DDRB = 0x00;
	DDRC = 0x00;
	DDRD = 0x00;
	
	PORTB = 0x00;
	PORTC = 0x00;
	PORTD = 0x00;
	
	// Init system's pins
	system_pins_init();
	
	// Init system's modules
	system_modules_init();
	
	// Init system interrupts's variables
	system_interrupts_variables_init();
	
	// Init system's interrupts
	system_interrupts_init();
	
    while (1)
    {
		// Service entire system's work
		service_system_work();
    }
}

