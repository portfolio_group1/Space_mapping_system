/*
* System_controller.c
*
* Created: 06/03/2021 18:40:22
*  Author: OKNO1010B
*/

#include <avr/interrupt.h>
#include <util/delay.h>

#include "System_controller.h"
#include "../User_control_panel/User_control_panel.h"
#include "../SPI_controller/SPI_controller.h"
#include "../Interrupts_controller/Interrupts_controller.h"

#include "../LCD_driver/LCD_driver.h"


void system_pins_init() {
	
	// Init User control panel's pins
	user_control_panel_pins_init();
	
	// Init SPI's pins
	spi_pins_init();
	
	// Init LCD driver's pins
	init_interface_lcd();
}

void system_modules_init() {
	
	// Init SPI module
	spi_init();
}

void system_interrupts_variables_init() {
	
	// Init SPI interrupts variables
	spi_interrupts_variables_init();
	
	// Init user control panel's variables
	user_control_panel_interrupts_variables_init();
}

void system_interrupts_init() {
	
	// Unlock uc's interrupts
	sei();
	
	// Unlock user control panel's interrupts
	user_control_panel_interrupts_unlock();
	
	// Unlock SPI's interrupts
	spi_interrupts_init();
}

void service_system_work() {
	
	// Service received SPI's frames sequences
	service_SPI_data();
	
	// Scan user control panel's instruction
	scan_user_control_panel_instructions();
}