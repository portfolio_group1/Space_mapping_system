/*
 * System_controller.h
 *
 * Created: 06/03/2021 18:40:40
 *  Author: OKNO1010B
 */ 


#ifndef SYSTEM_CONTROLLER_H_
#define SYSTEM_CONTROLLER_H_


// Init all system modules's pins
void system_pins_init();

/* Init all avr's modules
* SPI
*/
void system_modules_init();

// Init all system interrupts's variables
void system_interrupts_variables_init();

/* Unlock :
* uc'c interrupts
* User control panel pins's interrupts
* SPI's interrupts
*/
void system_interrupts_init();

// Service entire system's works
void service_system_work();

#endif /* SYSTEM_CONTROLLER_H_ */