/*
 * HD44780_driver.h
 *
 * Created: 14.03.2021 14:56:02
 *  Author: OKNO1010B
 *
 * Library based on:
 * Joerg Wunsch's hd44780 library
 * and
 * Tomasz Francuz exercises
 *
 * F_CPU should be defined in AVR/GNU C Compiler -> Symbols
 */ 


#ifndef HD44780_DRIVER_H_
#define HD44780_DRIVER_H_

#include <stdint.h>


/* ###!!!### Privates functions ###!!!### */

void lcd_puttext_P(const char __flash *txt);


/* ###!!!### Library interface ###!!!### */

/*///////////////////////////////////////////////////
	Init lcd
///////////////////////////////////////////////////*/
void lcd_init();

/*///////////////////////////////////////////////////
	Show single character on lcd
///////////////////////////////////////////////////*/
void lcd_putchar(char c);

/*///////////////////////////////////////////////////
	Show character string
	# '\n' in character string equal go to new line
///////////////////////////////////////////////////*/
void lcd_puttext(const char __memx *txt);

/*///////////////////////////////////////////////////
	Go cursor to respectively position
///////////////////////////////////////////////////*/
void lcd_goto(uint8_t x, uint8_t y);

/*///////////////////////////////////////////////////
	Clear lcd's content
///////////////////////////////////////////////////*/
void lcd_cls();

/*///////////////////////////////////////////////////
	Define single own character
	#  charno : character address in CGRAM (0 : 7 address value)
	# chardef : new character pixel's matrix (three first bit in each row is usually discriminate)
	# !!! After defined characters should cause lcd_goto function !!!
///////////////////////////////////////////////////*/
void lcd_defchar(uint8_t charno, const uint8_t *chardef);


#endif /* HD44780_DRIVER_H_ */