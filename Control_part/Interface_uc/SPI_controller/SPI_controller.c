/*
 * SPI_controller.c
 *
 * Created: 07/03/2021 11:19:58
 *  Author: OKNO1010B
 */ 

#include "avr/io.h"
#include "SPI_controller.h"
#include "../Interrupts_controller/Interrupts_controller.h"
#include "../Measurement_converter/Measurement_converter.h"
#include "../LCD_driver/LCD_driver.h"


void spi_pins_init() {
	
	/* Init SPI's pins
	* PB2 : cs (input) {pull up}
	* PB3 : mosi (input)
	* PB4 : miso (output)
	* PB5 : sck (input)
	*/
	DDRB &= 0xD3;
	DDRB |= 0x10;
	
	PORTB &= 0xC7;
	PORTB |= 0x04;
}

void spi_init() {
	
	/* Set SPI
	* MSB first (Big endian frame's data)
	* Slave mode
	* Polarity = high
	* Phase = 2 edge
	* Prescaler f_cpu / 4
	*/
	SPCR |= 0x4C;
}

void spi_interrupts_init() {
	
	// Unlock SPI's interrupts
	SPSR &= 0x00;
	SPCR |= 0x80;
}

void spi_interrupts_variables_init() {
	
	// Init SPI frames's counter
	SPI_data.SPI_frames_counter = 0;
}

void service_SPI_data() {
	
	// Check new frames sequence from SPI
	if (SPI_data.SPI_frames_counter == SPI_frames_size) {
		
		// Create distance data container
		Distance_data distance_data;

		// Convert measurement time to distance data form
		measurement_converter(SPI_data.SPI_frames[0], &distance_data);
		
		// Catch motor's angle
		distance_data.motor_angle = SPI_data.SPI_frames[1];
		
		// Show converted received data in LCD
		show_measurement_state(&distance_data);

		// Reset SPI frames's counter
		SPI_data.SPI_frames_counter = 0;
	}
}