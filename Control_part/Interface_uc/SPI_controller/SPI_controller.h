/*
 * SPI_controller.h
 *
 * Created: 07/03/2021 11:20:05
 *  Author: OKNO1010B
 *
 * Weight down pins
 * PB2 (cs) {pull up}
 * PB3 (mosi)
 * PB4 (miso)
 * PB5 (sck)
 */ 


#ifndef SPI_CONTROLLER_H_
#define SPI_CONTROLLER_H_

void spi_pins_init();

void spi_init();

void spi_interrupts_init();

void spi_interrupts_variables_init();

void service_SPI_data();

#endif /* SPI_CONTROLLER_H_ */