### It is the project of the space mapping system based on the 8-bit AVR microcontrollers. ###

---

### Brief description of the system
The system is used to carry out space mapping measurement.
Devices have been grouped into the operation part and the control part.

---

#### The devices included in the control part are used to
1. Presentation of the system data to the user,
2. Adaptation to the user requests (control panel unit),
2. Control the devices included in the operation part.

#### The devices included in the operation part are used to
1. Taking the measurement,
2. Changing the measurement direction,
3. Adaptation to the control part requests,
4. Sending the measurement data to the control part.

Both system parts communicate with each other using the SPI serial interface.
The Control part sends signals to the operation part in order to receive measurement data.
The system user can influence the system behaviour
(pausing the system, restarting the system, resuming system operation).

The operation part intercepts the control signals. It sets the position of the motor in order to set the measurement direction, further this one performs the measurement.
Once the measurement has been taken, the operation part sends the data to the control part. This information is presented in the system interface (LCD display).

---

### Catalogues
1. Schematics, here are schematics describing:
   - Deployed device : Main system preview (from the customer side),
   - System architecture schematic : Preview of the system parts from the generalized side,
   - Software architecture schematic : Presentation of all software components included in each microcontroller of the system (simultaneously for the operation part and control parts),
   - System state machines schematic : Description of the control flow in the whole system using the UML state diagram (both operation part and control part simultaneously),
2. Control part : Design of the control part based on the Atmega328P microcontroller (compatibility with the Atmel Studio IDE),
3. Operation part : Design of the operation part based on the Atmega328P microcontroller (compatibility with the Atmel Studio IDE).

---

### System devices
- Atmega328P microcontrollers,
- Ultrasonic sensor HC-SR04,
- Pololu A4988 motor driver,
- Stepper motor JK42HS40-0504,
- LCD display 2x16,
- Control panel (tact switches included).

---

### Tools used to implement the project
1. Atmel Studio IDE